class CartItemsController < ApplicationController
    def create
        if @cart_item.blank?
           @cart_item = current_cart.cart_items.build(product_id: params[:product_id])
        end
      
        @cart_item.save
        redirect_to current_cart
    end
    def destroy
        cart_item = CartItem.find(params[:id])
        cart_item.destroy
        redirect_to current_cart
    end
end

